#ifndef STREAM9_BITS_PREDICATE_HPP
#define STREAM9_BITS_PREDICATE_HPP

#include <concepts>
#include <type_traits>

namespace stream9::bits {

template<std::integral T, std::integral U>
constexpr bool
all_of(T v, U mask)
    noexcept(std::is_nothrow_convertible_v<U, T>)
    requires std::is_convertible_v<U, T>
{
    auto m = static_cast<T>(mask);

    return (v & m) == m;
}

template<std::integral T, std::integral U>
constexpr bool
any_of(T v, U mask)
    noexcept(std::is_nothrow_convertible_v<U, T>)
    requires std::is_convertible_v<U, T>
{
    auto m = static_cast<T>(mask);

    if (m == U()) return false;

    return (v & m) != T();
}

template<std::integral T, std::integral U>
constexpr bool
none_of(T v, U mask)
    noexcept(std::is_nothrow_convertible_v<U, T>)
    requires std::is_convertible_v<U, T>
{
    return !any_of(v, mask);
}

template<std::integral T, std::integral U>
constexpr bool
includes(T v, U mask)
    noexcept(std::is_nothrow_convertible_v<U, T>)
    requires std::is_convertible_v<U, T>
{
    return all_of(v, mask);
}

} // namespace stream9::bits

#endif // STREAM9_BITS_PREDICATE_HPP
