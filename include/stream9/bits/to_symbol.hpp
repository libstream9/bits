#ifndef STREAM9_BITS_TO_SYMBOL_HPP
#define STREAM9_BITS_TO_SYMBOL_HPP

#include "modifier.hpp"
#include "predicate.hpp"

#include <concepts>
#include <string>
#include <initializer_list>

namespace stream9::bits {

template<std::integral T>
struct flag_entry {
    T flag;
    char const* symbol;
};

#define STREAM9_FLAG_ENTRY(v) { v, #v }

template<std::integral T>
std::string
ored_flags_to_symbol(T flags, std::initializer_list<flag_entry<T>> entries)
{
    std::string s;
    bool first = true;

    for (auto const& e: entries) {
        if (includes(flags, e.flag)) {
            if (first) {
                first = false;
            }
            else {
                s.append(" | ");
            }

            s.append(e.symbol);

            reset(flags, e.flag);
        }
    }

    if (flags != 0 || s.empty()) {
        if (!first) s.append(" | ");

        s.append("unknown (");
        s.append(std::to_string(flags));
        s.push_back(')');
    }

    return s;
}

} // namespace stream9::bits

#endif // STREAM9_BITS_TO_SYMBOL_HPP
