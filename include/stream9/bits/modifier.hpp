#ifndef BITS_STREAM9_BITS_MODIFIER_HPP
#define BITS_STREAM9_BITS_MODIFIER_HPP

#include <concepts>
#include <type_traits>

namespace stream9::bits {

template<std::integral T, std::integral U>
constexpr void
set(T& v, U const mask)
    requires (!std::is_const_v<T>)
{
    v |= mask;
}

template<std::integral T, std::integral U>
constexpr void
reset(T& v, U const mask)
    requires (!std::is_const_v<T>)
{
    v &= ~mask;
}

} // namespace stream9::bits

#endif // BITS_STREAM9_BITS_MODIFIER_HPP
